#pragma once
#include <IO.hpp>
#include <noncopyable.hpp>

template <typename T, typename A = volatile T*, typename I = IO<T, A>>
class Register : public noncopyable {
    typedef T data_t;
    typedef A address_t;
    typedef I io_t;

public:
    constexpr Register(address_t address, data_t data = 0):m_address(address) {
        //m_io.Write(m_address, data);
    };

private:
    address_t   m_address;
    io_t        m_io;
};