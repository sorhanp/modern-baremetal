#pragma once

class noncopyable
{
protected:
    constexpr noncopyable() = default;
    constexpr ~noncopyable() = default;

    constexpr noncopyable(const noncopyable &) = delete;
    constexpr noncopyable &operator=(const noncopyable &) = delete;
};