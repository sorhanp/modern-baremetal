add_library(uart INTERFACE)
target_include_directories(uart INTERFACE .)

add_library(Uart1 Uart1.cpp)
target_link_libraries(Uart1
  PRIVATE
  # options
    baremetal_options
    project_options
    project_warnings

  # targets
  PUBLIC
    uart
    Register
)