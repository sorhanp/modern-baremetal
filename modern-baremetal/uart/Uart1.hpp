#pragma once

#include <IODefines.hpp>
#include <IUart.hpp>
#include <Register.hpp>

class Uart1: public IUart { 
public:
    Uart1();

private:
    typedef Register<int, IODefines::type> register_t;
    register_t enable;
};

