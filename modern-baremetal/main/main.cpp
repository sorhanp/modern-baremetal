#include <MainClass.hpp>

int main()
{
    MainClass main;
    main.Run();

    // Should not get here, return non standard
    return 1;
}