add_library(mainFolder INTERFACE)
target_include_directories(mainFolder INTERFACE .)

add_library(MainClass MainClass.cpp)
target_link_libraries(MainClass
  PRIVATE
  # options
    baremetal_options
    project_options
    project_warnings

  # targets
  PUBLIC
    mainFolder
    Uart1
)

add_library(main main.cpp)
target_link_libraries(main
  PRIVATE
  # options
    baremetal_options
    project_options
    project_warnings

  # targets
    MainClass
)