#pragma once
#include <Uart1.hpp>

class MainClass{
public:
    int Run();

private:
    Uart1 uart1;
};