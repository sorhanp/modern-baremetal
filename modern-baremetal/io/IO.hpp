#pragma once
#include <noncopyable.hpp>

template <typename T, typename>
class IO : public noncopyable {
    typedef T data_t;
    typedef volatile data_t register_t;
    typedef register_t *address_t;

public:
    constexpr auto Read(address_t address) {
        return *address;
    }

    constexpr auto Write(address_t address, data_t data) -> void {
        *address = data;
    }
};