#pragma once
// Defines for Raspberry Pi 3
// TODO: add for Pi 4



class IODefines {
    private:
    typedef unsigned int ioDefines_t;
    static constexpr ioDefines_t ioBaseAddress = 0x3F000000;


    public:
    // Do not allow object creation
    IODefines() = delete;
    ~IODefines() = delete;
    typedef ioDefines_t type;

    enum aux : type{
        enable      = (ioBaseAddress + 0x00215004),
        io          = (ioBaseAddress + 0x00215040),
        ier         = (ioBaseAddress + 0x00215044),
        iir         = (ioBaseAddress + 0x00215048),
        lcr         = (ioBaseAddress + 0x0021504C),
        mcr         = (ioBaseAddress + 0x00215050),
        lsr         = (ioBaseAddress + 0x00215054),
        msr         = (ioBaseAddress + 0x00215058),
        scratch     = (ioBaseAddress + 0x0021505C),
        cntl        = (ioBaseAddress + 0x00215060),
        stat        = (ioBaseAddress + 0x00215064),
        baud        = (ioBaseAddress + 0x00215068)
    };

};

/*enum class IORegister : ioRegisterType {
    gpfsel0     = (ioBaseAddress + 0x00200000),
    gpfsel1     = (ioBaseAddress + 0x00200004),
    gpfsel2     = (ioBaseAddress + 0x00200008),
    gpfsel3     = (ioBaseAddress + 0x0020000C),
    gpfsel4     = (ioBaseAddress + 0x00200010),
    gpfsel5     = (ioBaseAddress + 0x00200014),
    
    gpset0      = (ioBaseAddress + 0x0020001C),
    gpset1      = (ioBaseAddress + 0x00200020),

    gpclr0      = (ioBaseAddress + 0x00200028),
    
    gplev0      = (ioBaseAddress + 0x00200034),
    gplev1      = (ioBaseAddress + 0x00200038),

    gpeds0      = (ioBaseAddress + 0x00200040),
    gpeds1      = (ioBaseAddress + 0x00200044),

    gphen0      = (ioBaseAddress + 0x00200064),
    gphen1      = (ioBaseAddress + 0x00200068),

    gppud       = (ioBaseAddress + 0x00200094),

    gppudclk0   = (ioBaseAddress + 0x00200098),
    gppudclk1   = (ioBaseAddress + 0x0020009C)
};

enum class AUXRegister {
    enables     = (ioBaseAddress + 0x00215004),
    io          = (ioBaseAddress + 0x00215040),
    ier         = (ioBaseAddress + 0x00215044),
    iir         = (ioBaseAddress + 0x00215048),
    lcr         = (ioBaseAddress + 0x0021504C),
    mcr         = (ioBaseAddress + 0x00215050),
    lsr         = (ioBaseAddress + 0x00215054),
    msr         = (ioBaseAddress + 0x00215058),
    scratch     = (ioBaseAddress + 0x0021505C),
    cntl        = (ioBaseAddress + 0x00215060),
    stat        = (ioBaseAddress + 0x00215064),
    baud        = (ioBaseAddress + 0x00215068)
};*/